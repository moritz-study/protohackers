import asyncio
import sys

class ShittyEchoProtocol(asyncio.Protocol):
    
    def connection_made(self, transport: asyncio.BaseTransport) -> None:
        self.transport = transport
        self.peername = transport.get_extra_info("peername")
        print(f"connected to {self.peername}", flush=True)
    
    def connection_lost(self, exc: Exception | None) -> None:
        print(f"connection to {self.peername} lost", flush=True)
    
    def data_received(self, data: bytes) -> None:
        print(f"received data: {data}", flush=True)
        print(f"sending data: {data}", flush=True)
        self.transport.write(data)

    def eof_received(self) -> bool | None:
        print("received EOF, closing.")
        self.transport.close()

async def main(ip: str, port: int):
    loop = asyncio.get_running_loop()

    server = await loop.create_server(ShittyEchoProtocol, ip, port)
    await server.serve_forever()


if __name__ == "__main__":
    *_, ip, port = sys.argv
    print(f"starting echo server, trying to bind to {ip}:{port}")
    asyncio.run(main(ip, int(port)))